import matplotlib.pyplot as plt
import numpy as np
import mxnet as mx
import csv

from skimage.transform import resize
from skimage.color import rgb2grey
from mxnet.gluon import data as gdata


IMAGE_SIZE = 30
TEST_DATA_FACTOR = 0.2
TRACK_SIZE = 30
TRACK_TEST_SIZE = int(TRACK_SIZE * TEST_DATA_FACTOR)
AUGMENTATIONS_FOR_IMAGE = 30


# function for reading the images
# arguments: path to the traffic sign data, for example './GTSRB/Training'
# returns: list of images, list of corresponding labels
def read_traffic_signs(rootpath='GTSRB/Final_Training/Images/'):
    """
    Reads traffic sign data for German Traffic Sign Recognition Benchmark.

    :param rootpath: path to the traffic sign data, for example './GTSRB/Training'
    :return: list of images, list of corresponding labels
    """
    images = []  # images
    labels = []  # corresponding labels
    # loop over all 42 classes
    for c in range(0, 43):
        prefix = rootpath + '/' + format(c, '05d') + '/'  # subdirectory for class
        gt_file = open(prefix + 'GT-' + format(c, '05d') + '.csv')  # annotations file
        gt_reader = csv.reader(gt_file, delimiter=';')  # csv parser for annotations file
        next(gt_reader)  # skip header
        # loop over all images in current annotations file
        for row in gt_reader:
            images.append(plt.imread(prefix + row[0]))  # the 1th column is the filename
            labels.append(int(row[7]))  # the 8th column is the label
        gt_file.close()
    return images, labels


# calculating the width for padding
def calc_pad_width(img):
    h, w, _ = img.shape
    diff = abs(w - h)
    if h == w:
        return (0, 0), (0, 0), (0, 0)
    elif h > w:
        return (0, 0), ((diff + 1) // 2, diff // 2), (0, 0)
    else:
        return ((diff + 1) // 2, diff // 2), (0, 0), (0, 0)


# takes list of images and transforms them to the same size
# return the list of transformed messages
def transform_to_same_size(images_list):
    res = []
    for img in images_list:
        padded_img = np.pad(img, calc_pad_width(img), 'edge')  # padding
        res.append(resize(padded_img, (IMAGE_SIZE, IMAGE_SIZE)))  # resize
    return res


def split_data(dataset, classes):
    train_x = []
    train_y = []
    test_x = []
    test_y = []

    dataset_size = len(dataset)
    # separately divide each track into train and test dataset
    for index in range(0, dataset_size, TRACK_SIZE):
        track_data = dataset[index:index + TRACK_SIZE]  # slice the images set for current track
        track_classes = classes[index:index + TRACK_SIZE]  # slice the classes for current track
        track_train_x, track_train_y, track_test_x, track_test_y = split_data_in_track(track_data, track_classes)
        train_x += track_train_x
        train_y += track_train_y
        test_x += track_test_x
        test_y += track_test_y

    return train_x, train_y, test_x, test_y


def split_data_in_track(track, classes):
    train_x = []
    train_y = []
    test_x = []
    test_y = []

    # choose random images for test set
    for i in range(TRACK_TEST_SIZE):
        random_index = np.random.randint(0, TRACK_SIZE - i)
        test_x.append(track.pop(random_index))
        test_y.append(classes.pop(random_index))

    train_x += track
    train_y += classes

    return train_x, train_y, test_x, test_y


def count_frequencies(classes):
    values, counts = np.unique(classes, return_counts=True)
    # Uncomment below to show frequencies plot
    plt.figure(num=1, figsize=(12, 8), dpi=100)
    plt.bar(values, counts)
    plt.show()
    return values, counts


def apply_augmentation(img, aug=gdata.vision.transforms.RandomBrightness(0.5), amount=1):
    return [aug(mx.nd.array(img)).asnumpy() for _ in range(amount)]


def augment_data(images, img_classes, frequencies, frequencies_classes):
    """
    Augments data
    :param images: set of original images dataset
    :param img_classes: classes of images
    :param frequencies: amount of images in each class
    :param frequencies_classes: class of frequency
    :return: return list of images divided by classes
    """
    # Separate images by classes
    images_by_classes = [[] for _ in range(len(img_classes))]
    maximum_samples = max(frequencies)
    for i in range(len(images)):
        images_by_classes[img_classes[i]].append(images[i])

    for c in frequencies_classes:
        print(f'Augmenting class {c}')
        augmented_images = []

        # Count how many images do we need
        class_frequency_index = np.where(frequencies_classes == c)[0][0]
        difference = maximum_samples - frequencies[class_frequency_index]
        images_in_class = len(images_by_classes)

        # Apply augmentation to each image in class
        image_index = 0
        while difference - AUGMENTATIONS_FOR_IMAGE > 0:
            augmented_images += apply_augmentation(images_by_classes[c][image_index], amount=AUGMENTATIONS_FOR_IMAGE)
            difference -= AUGMENTATIONS_FOR_IMAGE
            image_index += 1
            if image_index >= images_in_class:
                image_index = 0

        # Count how many images left to add and apply augmentation to first image in class
        augmented_images += apply_augmentation(images_by_classes[c][0], amount=difference)

        # Add augmented images to original one
        images_by_classes[c] += augmented_images

    return images_by_classes


def normalize_images(images_by_classes):
    for c in range(len(images_by_classes)):
        for i in range(len(images_by_classes[c])):
            images_by_classes[c][i] = rgb2grey(images_by_classes[c][i])


if __name__ == '__main__':
    print('Reading data')
    training_images, training_labels = read_traffic_signs()
    print('Padding images')
    padded_images = transform_to_same_size(training_images)
    print('Splitting data')
    X, Y, test_X, test_Y = split_data(padded_images, training_labels)
    print('Counting frequencies')
    freq_classes, freq = count_frequencies(Y)
    print('Augmenting data')
    augmented_classes = augment_data(X, Y, freq, freq_classes)
    print('Normalizing images')
    normalize_images(augmented_classes)

    # TODO: image normalization
    # TODO: train and test
    pass
